// ------------------------------------------------------------------------
Coin Machine PHP Application
// ------------------------------------------------------------------------

The app accepts a monetary value in a variety of formats from a user input.The value is then validated and parsed and the app will calculate the fewest coins required to achieve that value.

// ------------------------------------------------------------------------
Installation
// ------------------------------------------------------------------------

Just place the application on your server, no configuration required.

// ------------------------------------------------------------------------
Notes
// ------------------------------------------------------------------------

My regex is a little weak and I'm not too happy with my solution to the validation issue, but I've tested the application against the demo values and it works.

I'm not too sure how long I spent on this as I had to stagger it over a couple of days to find time, but if I find more time I will
implement AJAX to make the experience smoother.

Thanks for the challenge, I really enjoyed it! If I've done anything ridiculous in this project, or there were more appropriate answers to the problem, please let me know.

Contact: dalewebster48@gmail.com