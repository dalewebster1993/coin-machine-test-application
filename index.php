<!DOCTYPE html>
<?php 
	if ( ! empty( $_POST[ 'val' ] ) )
	{
		// The user has submitted the form
		// Load the classes required

		require_once "classes/Validator.php";
		require_once "classes/CTIConverter.php";
		require_once "classes/CoinMachine.php";

		$val = $_POST[ 'val' ];
		$v = new Validator( $_POST[ 'val' ] );

		// Validate the input
		if ( $v->validate() )
		{
			// The input value was fine
			// Convert the monetary value into a numeric value
			$converter = new CTIConverter( $_POST[ 'val' ] );
			$numeric = $converter->getNumericValue();

			if ( ! $numeric ) 
			{
				// There was an error in the conversion process
				$conversionErrors = $converter->errors;
			}
			else
			{	
				// Create CoinMachine instance
				$coinMachine = new CoinMachine( $numeric );

				// Get the required coins
				$requiredCoins = $coinMachine->calculateRequiredCoins();
			}
		}
		else
		{
			$valErrors = $v->errors;
		}
	}
?>
<html>
<head>
	<meta charset = "utf8" />
	<title>Coin Machine</title>

	<!-- Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.css" rel="stylesheet">
	
	<!-- Style -->
	<link rel="stylesheet" href="css/style.css">

	<!-- JQuery -->
	<script src = "http://code.jquery.com/jquery-2.1.3.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Coin Machine</h1>
		<div class="row">
			<div class="col-md-6">
				<form id = 'currency-calc' action = "" method = "POST" >
					<div class="form-group">
						<input type = 'text' name = 'val' class = 'form-control' id = 'currency-calc-val' placeholder = "£10.56" />
						<input type = 'submit' class = 'form-control' value = "Calculate Required Coins" />
					</div>
				</form>
				<div class="clearfix"></div>
				<div class="errors">
					<?php if ( isset( $valErrors ) ): ?>
						<ul id = 'validation-errors'>
							<?php foreach ( $valErrors as $err ): ?>
								<li>
									<?= $err ?>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<?php if ( isset( $conversionErrors ) ): ?>
						<ul>
							<?php foreach ( $conversionErrors as $err ): ?>
								<li>
									<?= $err ?>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
				<div class="results">
					<?php if ( isset( $requiredCoins ) ): ?>
						<h3>
							To Make <?= $val ?>
						</h3>
						<?php foreach ( $requiredCoins as $coin ): ?>
							<p>
								<?= $coin; ?>
							</p>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-6">
				<p>
					<ol>
						<li>
							Enter a monetary value in the box to the left.
						</li>
						<li>
							Click 'Calculate Required Coins'.
						</li>
						<li>
							The application will calculate how to achieve this value with the 
							fewest coins.
						</li>
					</ol>
				</p>
			</div>
		</div>
	</div>
</body>
</html>