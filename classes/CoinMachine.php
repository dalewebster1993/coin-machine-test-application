<?php
class CoinMachine
{

	// ------------------------------------------------------------------------

	/**
	* Decimal value to be evaluated
	*/
	protected $value;

	/**
	* Array of coin values used
	*/
	protected $acceptedCoins;

	// ------------------------------------------------------------------------

	/**
	* Constructor
	* @param int $value - The Value to be evaluated
	*/
	public function __construct ( $value )
	{
		// Create the acceptedCoins array
		$this->acceptedCoins = [
			"£2"		=>		200,
			"£1"		=>		100,
			"50p"		=>		50,
			"20p"		=>		20,
			"10p"		=>		10,
			"5p"		=>		5,
			"2p"		=>		2,
			"1p"		=>		1
		];

		// Set the value
		if ( is_numeric( $value ) ) $this->value = $value;
	}

	// ------------------------------------------------------------------------

	/**
	* Calculate Required Coins
	* @return Array
	*/
	public function calculateRequiredCoins ()
	{
		if ( ! $this->value )
		{
			// Can't calculate when there isn't a value!
			return [];
		}
		else
		{
			// The result
			$requiredCoins = [];

			// Starting value
			$startingValue = $this->value;

			// Remaining value
			$remainingValue = $this->value;

			// Cycle through the accepted coins
			foreach ( $this->acceptedCoins as $name => $coinValue )
			{
				if ( $remainingValue == $coinValue )
				{
					// This coin is a perfect match
					$requiredCoins[] = "1 X $name" ;
					break;
				}
				elseif ( $coinValue > $remainingValue )
				{
					// This coin is USELESS!
					continue;
				}
				else
				{
					// The coin can be used to make up the remaining amount
					// Find the number of times this coin can be used
					$uses = floor( $remainingValue / $coinValue );

					// Enter this in the required coins array
					$requiredCoins[] = "$uses X $name";

					// Calculate the remainder required
					$remainingValue %= $coinValue;
				}
			}

			return $requiredCoins;

		}
	}
	
	// ------------------------------------------------------------------------

}