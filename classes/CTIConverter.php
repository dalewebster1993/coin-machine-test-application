<?php
class CTIConverter
{
	// ------------------------------------------------------------------------
	// Currency To Integer Converter
	// ------------------------------------------------------------------------

	/**
	* Errors encountered during the conversion process
	*/
	public $errors;

	/**
	* Monetary value in string format
	*/
	protected $string;

	/**
	* Regex Pattern
	*/
	public $format;

	/**
	* Accepted characters
	*/
	protected $acceptedCharacters;


	// ------------------------------------------------------------------------

	/**
	* Constructor
	*/
	public function __construct ( $string )
	{
		// Set the string
		$this->string = $string;

		// Create the regex pattern
		// $this->format = "/^((£)?[0-9]+(?!p))?(?:\.[0-9]{2}){0,1}(p)?$/";
		// $this->format = "/^((£?[0-9]+)(?!p)|([0-9]+p?))?(\.[0-9]{2}(p)?)?$/";
		// $this->format = "/^((£?[0-9]+)(?!p)|([0-9]+p?$))?(\.[0-9]+(p)?)?$/";
		$this->format = "/^((£?[0-9]+)(?!p)|([0-9]+p?))?(?<!p)(\.[0-9]+)?p?$/";

		// Create the accepted characters
		$this->acceptedCharacters = [ "£" , "p" , "." ];

		// Prepare errors array
		$this->errors = [];
	}

	// ------------------------------------------------------------------------

	/**
	* Get Numeric Value
	* @return int
	*/
	public function getNumericValue ()
	{
		if ( ! $this->string )
		{
			return FALSE;
		}
		else
		{
			// Check the format of the input
			if ( ! preg_match( $this->format , $this->string ) )
			{
				// The string is an invalid format
				$this->errors[] = "The input is an invalid format";
				return FALSE;
			}
			else
			{
				// The input is a valid format
				// Check for a decimal point
				if ( ! $this->contains_char( "." , $this->string ) )
				{
					// There is no decimal point
					if ( $this->contains_char( "£" , $this->string ) )
					{
						return (int) $this->filterAcceptedCharacters( $this->string ) * 100;
					}
					else 
					{
						// The value will be treated as pence
						return (int) $this->string;
					}
				}
				else
				{
					// Remove any £ and p signs from the string
					$s = $this->remove_char( "£" , $this->string );
					$s = $this->remove_char( "p" , $s );

					// Make sure that the value is correctly formatted
					$s = number_format( $s , 2 );

					// Get pounds and pence in an array
					$a = explode( "." , $s );
					$pounds = $a[0];
					$pence = $a[1];

					// Remove any approved characters
					$pounds = (int) $this->filterAcceptedCharacters( $pounds );
					$pence = (int) $this->filterAcceptedCharacters( $pence );

					return (int) ( $pounds * 100 ) + $pence;
				}

			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	* Filter Accepted Characters
	* @param String $source - The string to filter
	* @return String
	*/
	private function filterAcceptedCharacters ( $source )
	{
		foreach ( $this->acceptedCharacters as $char )
		{
			$source = $this->remove_char( $char , $source );
		}

		return $source;
	}

	// ------------------------------------------------------------------------

	/**
	* Contains Character
	* @param String $char - The character to look for in the string
	* @param String $source - The string to check
	* @return BOOL
	*/
	private function contains_char ( $char , $source )
	{
		if ( ! $source )
		{
			return FALSE;
		}
		else
		{
			// return strpos( $source ,  $char );
			return ( strpos( $source , $char ) !== FALSE );
		}
	}

	// ------------------------------------------------------------------------

	/**
	* Remove Character
	* @param String $char - The character to remove from the string
	* @param String $source - The string to action
	* @return String
	*/
	private function remove_char ( $char , $source )
	{
		if ( ! $source )
		{
			return FALSE;
		}
		else
		{
			return str_replace( $char , "" ,  $source );	
		}
	}

	// ------------------------------------------------------------------------

}