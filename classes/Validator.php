<?php
class Validator
{

	// ------------------------------------------------------------------------

	/**
	* The value to be validated
	*/
	protected $value;

	/**
	* Array of errors form validation
	*/
	protected $errors;

	// ------------------------------------------------------------------------

	/**
	* Constructor
	* @param String $value - The value to be validated
	*/
	public function __construct ( $value )
	{
		// Set the value
		$this->value = $value;
	}

	// ------------------------------------------------------------------------

	/**
	* Validate
	* @return BOOL
	*/
	public function validate ()
	{
		if ( ! $this->value )
		{
			$this->errors[] = "Required field is empty";
			return FALSE;
		}
		elseif ( ! is_string( $this->value ) ) 
		{
			$this->errors[] = "The value must be a String";
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	// ------------------------------------------------------------------------

}