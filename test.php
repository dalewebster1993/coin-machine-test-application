<!DOCTYPE html>
<html>
<head>
	<meta charset = "utf8" />
	<title>Test</title>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<?php
	require_once "classes/CoinMachine.php";
	require_once "classes/CTIConverter.php";

?>
<pre>
	<?php
		$c = new CTIConverter( "£12.32p" );
		$res = $c->getNumericValue();
		if ( ! $res )
		{
			print_r( $c->errors );
		}
		else
		{
			echo $res;
		}
	?>
</pre>
</body>
</html>